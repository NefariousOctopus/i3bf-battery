# i3bf-battery - i3barfodder battery plugin
A simple [i3barfodder](https://gitlab.com/rndusr/i3barfodder) battery plugin.


## Disclaimer

This is fast dirty 'works-for-me' script. It could do anything, including eating your socks :)


## Install

Just put the script somewhere in your `$PATH` and make it executable. The following snippet assumes that `~/bin` is in your `$PATH`.

```bash
$ git clone git@gitlab.com:NefariousOctopus/i3bf-battery.git
$ cd i3bf-battery
$ chmod u+x ./i3bf-battery
$ cp i3bf-battery ~/bin
```


## Configuration

Example config.

```
[battery]
command = i3bf-battery
BATTERY_PATH = /sys/class/power_supply/BAT0/capacity
AC_ONLINE_PATH = /sys/class/power_supply/AC/online
COLORS = #F20-#09B
```

For more see [original project](https://gitlab.com/rndusr/i3barfodder).

## Status

- Currently getting the data by polling `BATTERY_PATH` and 'AC_ONLINE_PATH'.
- Udev event solution is on its way, but there are still few problems.
